@extends('layouts.layout')
@section('title')
    Publicar aviso - Fisica
@endsection
@section('content')
    <form action=" {{ route ('avisos.store') }} " method="POST" enctype="multipart/form-data">
        
        @include('layouts.posts-form')
        
        <div class="buttons buttons-post">
            <button class="button is-link total-width" type="submit">Registrar</button>
            <button class="button is-light total-width">Cancelar</button>
        </div>
    </form>
@endsection