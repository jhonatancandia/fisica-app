@extends('layouts.layout')
@section('title')
    Avisos - Física
@endsection
@section('content')
    @guest
        @else
        <a href="{{ route('avisos.create') }}" class="button is-success mb-10 total-width">Publicar avisos</a>   
    @endguest
    @foreach ($avisos as $aviso)
    <div class="is-full card pd-20 c-b bx-sh-none">
        <div class="columns">
            <div class="column">
                <h2 class="subtitle">{{ $aviso->title }}</h2>
                <span class="help">{{ $aviso->created_at->diffForHumans() }}</span>
            </div>
            @guest
                @else
                <div class="column">
                    <div class="buttons float-right buttons-post">
                        <a class="button is-link total-width" href="{{ route('avisos.edit', $aviso->slug) }}">Editar</a>
                        <form action="{{ route('avisos.destroy', $aviso->slug) }}" method="POST">
                            @csrf @method('DELETE')
                            <button class="button is-danger total-width">Eliminar</button>
                        </form>
                    </div>
                </div>
            @endguest
        </div>
        @if ($aviso->file)
            <p class="text-center">
                <img class="img-post" loading="lazy" src="{{ Storage::url($aviso->file) }}" alt="{{ $aviso->slug }}">
            </p>
        @endif
        <a href="{{ route('avisos.show', $aviso->slug) }}">Leer más...</a>
    </div>
    <hr>    
    @endforeach
    {{ $avisos->render() }}
@endsection
