@extends('layouts.layout')
@section('title')
    Editar aviso - Fisica
@endsection
@section('content')
    <form action=" {{ route ('avisos.update', $aviso->slug) }} " method="POST">
            
        @method('PUT')

        @include('layouts.posts-form')
        
        <div class="buttons buttons-post">
            <button class="button is-link total-width" type="submit">Editar</button>
            <button class="button is-light total-width">Cancelar</button>
        </div>
    </form>
@endsection