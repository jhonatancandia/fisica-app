@extends('layouts.layout')
@section('title')
    Mostrar aviso - Fisica
@endsection
@section('content')
    <div class="is-full">
        <h1 class="has-text-centered title">{{ $aviso->title }}</h1>
        <p class="text-center"> 
            @if ($aviso->file)
                <img src="{{ Storage::url($aviso->file) }}" alt="{{ $aviso->slug }}">
            @endif
        </p>
        <p>{{ $aviso->description }}</p>
    </div>
@endsection