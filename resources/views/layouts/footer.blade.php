<footer class="footer color-main has-text-centered has-text-white">
    <p>
        <span class="text-bold">Contacto: </span>fisica@fcyt.umss.edu.bo <span class="text-bold">Teléfono: </span>591 4 231765 <span class="text-bold">Interno: </span>320
    </p>
    <p>
        <span class="text-bold">Otros sitios de interés: </span>
        <a target="blank_" href="http://www.umss.edu.bo/">UMSS</a> | <a target="blank_" href="http://websis.umss.edu.bo/">WebSISS</a> | <a target="blank_" href="http://fcyt.umss.edu.bo/">FCyT</a> | <a target="blank_" href="http://enlinea.umss.edu.bo/">Plataformas virtuales</a>
    </p>
    <p>Universidad Mayor de San Simon. Carrera de Física - 2020.</<p>
</footer>