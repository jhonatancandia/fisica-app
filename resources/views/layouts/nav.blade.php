<nav class="navbar is-transparent has-shadow is-spaced">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ route('welcome') }}">
            <img src="{{ asset('img/logo.jpg') }}" alt="Logo Fisica"><span id="main">Física</span>
        </a>
        <div class="navbar-burger burger" data-target="navbar-main">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div id="navbar-main" class="navbar-menu">
        <div class="navbar-end">
            <a class="navbar-item is-hoverable {{ isActive('welcome') }}" href="{{ route('welcome') }}">
                Inicio
            </a>

            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    Información Académica
                </a>
                <div class="navbar-dropdown is-boxed is-right">
                    <a class="navbar-item {{ isActive('mision') }}" href="{{ route('mision') }}">
                        Misión y Visión
                    </a>
                    <a class="navbar-item {{ isActive('objetivos') }}" href="{{ route('objetivos') }}">
                        Objetivos
                    </a>
                    <a class="navbar-item {{ isActive('sociedad') }}" href="{{ route('sociedad') }}">
                        Sociedad Cientifica
                    </a>
                </div>
            </div>
            <a class="navbar-item is-hoverable {{ isActive('postgrado') }}" href="{{ route('postgrado') }}">
                Postgrado
            </a>
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    Proyectos
                </a>
                <div class="navbar-dropdown is-boxed is-right">
                    <a class="navbar-item {{ isActive('microscopia') }}" href="{{ route('microscopia') }}">
                        Microscopia electronica
                    </a>
                    <a class="navbar-item {{ isActive('modelacion') }}" href="{{ route('modelacion') }}">
                        Modelación Numerica de Variables Climaticas
                    </a>
                    <a class="navbar-item {{ isActive('ruido') }}" href="{{ route('ruido') }}">
                        Medición de Ruido Acústico
                    </a>
                    <a class="navbar-item {{ isActive('astrofisica') }}" href="{{ route('astrofisica') }}">
                        Astrofísica
                    </a>
                </div>
            </div>

            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    Interacción
                </a>
                <div class="navbar-dropdown is-boxed is-right">
                    <a class="navbar-item {{ isActive('museo') }}" href="{{ route('museo') }}">
                        Museo
                    </a>
                </div>
            </div>
            @guest
                <a class="navbar-item is-hoverable {{ isActive('avisos') }}" href="{{ route('avisos') }}">
                    Avisos
                </a>
                <a class="navbar-item is-hoverable" href="{{ route('login') }}">Ingresar</a>
            @else
                <a class="navbar-item is-hoverable {{ isActive('avisos') }}" href="{{ route('avisos.user') }}">
                    Avisos
                </a>
                <a class="navbar-item is-hoverable" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Salir</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>    
            @endguest
        </div>
    </div>
</nav>