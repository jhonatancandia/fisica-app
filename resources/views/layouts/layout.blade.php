<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description"
        content="Página Web de la Carrera de Física de la Universidad Mayor de San Simón, Facultad de Ciencias y Técnologia" />
    <meta name="keywords" content="fcyt fisica, web fisica umss, fisica umss, sitio web fisica, carrera fisica umss" />
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('css/bulma.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/icon.png') }}" type="image/x-icon">
</head>

<body>
    @include('layouts.nav')
    <div>
        @yield('carousel')
    </div>
    <section class="section">
        <div class="container">
            @yield('content')
        </div>
    </section>

    @include('layouts.footer')

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
</body>

</html>
