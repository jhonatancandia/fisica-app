@csrf
<div class="field">
    <label class="label">Título</label>
    <div class="control">
        <input class="input" type="text" value="{{ $aviso->title }}" name="title" >
    </div>
    <p class="help is-danger">{{ $errors->first('title') }}</p>
</div>

<div class="field">
    <label class="label">Descripción</label>
    <div class="control">
        <textarea class="textarea" name="description">{{ $aviso->description }}</textarea>
    </div>
    <p class="help is-danger">{{ $errors->first('description') }}</p>
</div>

<div class="file has-name is-fullwidth mb-10" id="file-form">
    <label class="file-label">
        <input class="file-input" type="file" name="file">
        <span class="file-cta">
            <span class="file-label">
                Escoge una imagen…
            </span>
        </span>
        <span class="file-name">
            Debe seleccionar un archivo
        </span>
    </label>
</div>

<div class="field">
    <div class="control">
        <label class="checkbox">
        <input type="checkbox" name="checked" >
            La información que ingreso es correcta.
        </label>
    </div>
    <p class="help is-danger">{{ $errors->first('checked') }}</p>
</div>

<script>
    const fileInput = document.querySelector('#file-form input[type=file]');
    fileInput.onchange = () => {
        if (fileInput.files.length > 0) {
            const fileName = document.querySelector('#file-form .file-name');
            fileName.textContent = fileInput.files[0].name;
        }
    }
</script>