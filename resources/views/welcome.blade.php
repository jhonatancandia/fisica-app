@extends('layouts.layout')
@section('title')
    Carrera Física - Universidad Mayor de San Simón
@endsection
@section('carousel')
    <div class="slideshow-container">
        <div class="slide fade">
            <div class="numbertext">1 / 1</div>
            <img src="{{ asset('img/car1.jpg') }}">
            <div class="text">Laboratorios de la Carrera de Física</div>
        </div>
        <a class="prev">&#10094;</a>
        <a class="next">&#10095;</a>
    </div>
@endsection    
@section('content')
    <h1 class="text-center mb-10">Eventos y actividades</h1>
    <div class="columns">
        @foreach ($avisos as $aviso)
        <div class="column">
            <div class="is-full card pd-20 c-b bx-sh-none">
                <div class="columns">
                    <div class="column">
                        <h2 class="subtitle">{{ $aviso->title }}</h2>
                    </div>
                </div>
                @if ($aviso->file)
                    <img class="img-post" loading="lazy" src="{{ Storage::url($aviso->file) }}" alt="{{ $aviso->slug }}">    
                @endif
                <a href="{{ route('avisos') }}">Leer más...</a>
            </div>
        </div>
        @endforeach
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/carousel.js') }}"></script>
@endsection