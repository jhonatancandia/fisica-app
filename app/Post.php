<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{

    //Restringimos unicamente a que se haga uso de estas columnas
    protected $fillable = [
        'user_id', 'title', 'description', 'slug', 'status', 'file'
    ];
    
    //Un post pertenece a un usuario
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
