<?php

namespace App\Http\Controllers\Avisos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use App\Http\Requests\SavePostRequest;
use Illuminate\Support\Facades\Auth;

class AvisoController extends Controller
{

    public function index()
    {
        $avisos = Post::orderBy('id', 'DESC')->where('status', 'PUBLISHED')->paginate(5);
        return view('avisos.index', compact('avisos'));
    }

    public function show($slug)
    {
        $aviso = Post::where('slug', $slug)->firstOrFail();
        return view('avisos.show', compact('aviso'));
    }

    public function edit($slug)
    {
        $aviso = Post::where('slug', $slug)->firstOrFail();
        return view('avisos.edit', compact('aviso'));
    }

    public function create()
    {
        return view('avisos.create', ['aviso' => new Post]);
    }

    public function store(SavePostRequest $request)
    {
        $user = User::where('email', auth()->user()->email)->firstOrFail();
        $user_id = $user->id;
        $file = '';
        
        if(request('file'))
        {
            $file = request('file')->store('public');
        }
        
        Post::create([
            'user_id' => $user_id,
            'title' => request('title'),
            'description' => request('description'),
            'slug' => str_slug(request('title')),
            'status' => 'PUBLISHED',
            'file' => $file
        ]);
        
        return redirect()->route('avisos')->with('info', 'Aviso publicado correctamente');
    }

    public function update(SavePostRequest $request, $slug)
    {
        $aviso = Post::where('slug', $slug)->first();
        $aviso->update([
            'title' => request('title'),
            'description' => request('description'),
            'slug' => str_slug(request('title'))
        ]);
    
        return redirect()->route('avisos.edit')->with('info', 'Aviso editado correctamente');
    }

    public function destroy($slug)
    {  
        Post::where('slug', $slug)->first()->delete();
        return redirect()->route('avisos')->with('info', 'Aviso eliminado correctamente');
    }

    public function getAvisosByIdUser() 
    {
        $idUser = Auth::id();
        $avisos = Post::orderBy('id', 'DESC')
                        ->where('user_id', $idUser)
                        ->where('status', 'PUBLISHED')
                        ->paginate(5);
        
        return view('avisos.index', compact('avisos'));
    }
}
