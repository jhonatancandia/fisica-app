<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class WelcomeController extends Controller
{
    public function index()
    {
        $avisos = Post::orderBy('id', 'DESC')->where('status', 'PUBLISHED')->paginate(3);
        return view('welcome', compact('avisos'));
    }
}
