<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|
| Public Routes
|
*/  

Route::get('', 'WelcomeController@index')->name('welcome');

// Informacion academica
Route::view('mision-vision', 'public.inf-aca.mision-vision')->name('mision');
Route::view('objetivos', 'public.inf-aca.objetivos')->name('objetivos');
Route::view('sociedad', 'public.inf-aca.sociedad')->name('sociedad');

//Proyectos 
Route::view('microscopia', 'public.proyectos.microscopia')->name('microscopia');
Route::view('modelacion', 'public.proyectos.modelacion')->name('modelacion');
Route::view('ruido', 'public.proyectos.ruido')->name('ruido');
Route::view('astrofisica', 'public.proyectos.astrofisica')->name('astrofisica');

// Interaccion
Route::view('museo', 'public.interaccion.museo')->name('museo');

// Avisos
Route::get('avisos', 'Avisos\AvisoController@index')->name('avisos');
Route::get('aviso/{slug}', 'Avisos\AvisoController@show')->name('avisos.show');

// Postgrado
Route::view('postgrado', 'public.postgrado.postgrado')->name('postgrado');

/*
|
| Teachers Routes
|
*/  

Route::group(['middleware' => ['auth']], function () {
    Route::get('avisos/create', 'Avisos\AvisoController@create')->name('avisos.create');
    Route::get('avisos/user', 'Avisos\AvisoController@getAvisosByIdUser')->name('avisos.user');;

    Route::get('avisos/edit/{slug}', 'Avisos\AvisoController@edit')->name('avisos.edit');
    Route::put('avisos/edit/{slug}', 'Avisos\AvisoController@update')->name('avisos.update');
    
    Route::post('avisos/store', 'Avisos\AvisoController@store')->name('avisos.store');

    Route::delete('avisos/{slug}', 'Avisos\AvisoController@destroy')->name('avisos.destroy');

    Route::view('admin', 'admin.index')->name('admin.index');
});

/*
|
| Admin Routes
|
*/  

Auth::routes();


