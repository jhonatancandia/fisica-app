<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     public function run()
    {
        factory(User::class, 29)->create();
        
        User::create([
            'name' => 'Jhonatan Candia Romero',
            'email' => 'tar-jhonatan@hotmail.com',
            'password' => bcrypt('jonas')
        ]);
    }
}
