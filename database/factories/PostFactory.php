<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Post;

$factory->define(Post::class, function (Faker $faker) {
    
    return [
        'user_id' => rand(1, 30),
        'title' => $faker->sentence(4),
        'description' => $faker->text(200),
        'slug' => str_slug($faker->sentence(4)),
        'status' => $faker->randomElement(['DRAFT', 'PUBLISHED']),
        'file' => $faker->imageUrl($width = 1200, $height = 400)
    ];
    
});
